#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "adicionar.h"
#define max 20

void escreve_fich_viagem(char *origem, char *destino, char *data, int hora, int minutos, int lugares)
{
    FILE *fp;
    long tam = 0, num = 0;
    int cod = 0;
    char orig[max], dest[max], dat[11];

    strcpy(orig, origem); /* tentar fazer funcao sem auxilio destes strcpy */
    strcpy(dest, destino);
    strcpy(dat, data);

    fp = fopen("viagens.txt", "r");

    tam = tamanho_fich(fp);

    while (num < tam)
        {
            cod = le_linha(fp, &num); /*para saber codigo a atribuir a novo cliente*/
        }
    fclose(fp);

    cod++;

    fp = fopen("viagens.txt", "a");

    fprintf(fp, "%d;%s;%s;%s;", cod, orig, dest, dat);

    if (hora < 10)
        fprintf(fp, "0%d:", hora);
    else
        fprintf(fp, "%d:", hora);

    if (minutos < 10)
        fprintf(fp, "0%d;%d;0;0;\n", minutos, lugares);

    else
        fprintf(fp, "%d;%d;0;0;\n", minutos, lugares);

    fclose(fp);
}

int Adicionar_Viagem()
{
    char origem[max], destino[max], data[11], c;
    int lugares, opcao = 1, horas, minutos;
    getchar();
    printf("Insira origem viagem:\n");
    scanf("%[^\n]s", origem);
    getchar();
    printf("Insira destino viagem:\n");
    scanf("%[^\n]s", destino);

    while (opcao == 1)
    {
        printf("Insira a data viagem (aaaa/mm/dd):\n");
        scanf("%s", data);

        if (strlen(data) == 10 && data[4]=='/' && data[7]=='/')
        {
            while (opcao == 1)
            {
                printf("Insira a hora viagem(hh:mm):\n");
                scanf("%d%c%d", &horas, &c, &minutos);
                if (c == ':' && horas>=0 && horas<24 && minutos>=0 && minutos<60)
                {
                    while (opcao == 1)
                    {
                        printf("Insira o total de lugares da viagem:\n");
                        scanf("%d", &lugares);
                        if (lugares > 0)
                        {
                            escreve_fich_viagem(origem, destino, data, horas, minutos, lugares);
                            opcao = 0;
                        }
                        else
                        {
                            opcao =0;
                            printf("Inseriu numero de lugares invalido");
                            while (opcao != 1)
                            {
                                printf("\nDeseja voltar o numero de lugares? (0-Nao / 1-Sim)\n");
                                scanf("%d", &opcao);
                                if (opcao == 0)
                                    return 0;
                                else if( opcao != 1)
                                    printf("Inseriu uma opcao invalida");

                            }
                        }

                    }
                }
                else
                {
                    opcao = 0;
                    printf("Inseriu uma hora invalida\n");
                    while (opcao != 1)
                    {
                        printf("\nDeseja voltar a escrever a hora? (0-Nao / 1-Sim)\n");
                        scanf("%d", &opcao);
                        if (opcao == 0)
                            return 0;
                        else if( opcao != 1)
                            printf("Inseriu uma opcao invalida");

                    }
                }
            }
        }
        else
        {
            opcao =0;
            printf("Inseriu uma data invalida");
            while (opcao != 1)
            {
                printf("\nDeseja voltar a escrever a data? (0-Nao / 1-Sim)\n");
                scanf("%d", &opcao);
                if (opcao == 0)
                    return 0;
                else if( opcao != 1)
                    printf("Inseriu uma opcao invalida");

            }
        }
    }

    printf("Viagem adicionada\n");
    getchar();
    getchar();

    return 0;
}
